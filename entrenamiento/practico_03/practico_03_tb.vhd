-------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.std_logic_unsigned.all;
USE ieee.numeric_std.ALL;
 
ENTITY deco_bcd_TB IS
END deco_bcd_TB;
 
ARCHITECTURE behavior OF deco_bcd_TB IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT deco_bcd
    PORT(
         clock: IN  std_logic;
         reset : IN  std_logic;
         input : IN  std_logic_vector(3 downto 0);
         salida : OUT  std_logic_vector(9 downto 0)
        );
    END COMPONENT;
    

    --Declaración de constantes.
    constant TbPeriod         : time    := 10 ms;


   --Inputs
   signal input : std_logic_vector(3 downto 0) := "0000";
   signal reset : std_logic := '0';
   signal clock : std_logic := '0';


   --Outputs
   signal salida : std_logic_vector(9 downto 0);

 
BEGIN
 
  -- Instantiate the Unit Under Test (UUT)
   uut: deco_bcd PORT MAP (
         input => input,
         reset => reset,
         clock => clock,
         salida => salida
        );


    --Proceso de generación de clock.
    clockGeneration : process
    begin
        clock <= '1';
        wait for TbPeriod/2;
        clock <= '0';
        wait for TbPeriod/2;
        
    end process clockGeneration;

   --Proceso de aplicación de estímulos.
   stim_proc: process
   begin   
   	  --- Estado inicial: 
	  input <= "0000"; 
      reset <= '0';
      clock <= '0';
      
      --- Estimulos      
      wait for (2)*TbPeriod;
      input <= "0001"; 
        
      wait for (2)*TbPeriod;
      input <= "0010"; 
        
      wait for (2)*TbPeriod;
      input <= "0011"; 
      
      wait for (2)*TbPeriod;
      input <= "0100"; 
      
      wait for (2)*TbPeriod;
      input <= "0101"; 
      
      wait for (2)*TbPeriod;
      input <= "0110"; 
      
      wait for (2)*TbPeriod;
      input <= "0111"; 
      
      wait for (2)*TbPeriod;
      input <= "1000"; 
      
      wait for (2)*TbPeriod;
      input <= "1001"; 
      
      wait for (2)*TbPeriod;
      input <= "1010"; 
      
      wait for (2)*TbPeriod;
      input <= "1011"; 
      
      wait for (2)*TbPeriod;
      input <= "1100"; 
      
      ---reset
      wait for 10 ms;
      reset <= '0';

      wait for (2)*TbPeriod;
      input <= "0001"; 
      
      wait for (2)*TbPeriod;
      input <= "0010"; 

      wait for (2)*TbPeriod; 


      wait;
   end process;

END;
