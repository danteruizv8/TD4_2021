----------------------------------------------------------------------------------
-- UNIVERSIDAD TECNOLÓGICA NACIONAL -FACULTAD REGIONAL CORDOBA
-- Carrera: INGENIERIA ELECTRÓNICA.
-- Asignatura: TÉCNICAS DIGITALES IV (ELECTIVA).
-- Año: 2021.
-- Grupo nº: XX.
-- Integrantes:
--
--		Ruiz, Dante Nelson				 Leg. nº:49881
--		Albarran, Gustavo.   		  	 Leg. nº:43143

--		
--
-- Fecha de Entrega: xx/xx/2021.
-- Hardware: Edu FPGA
-- VHDL auxiliares utilizados
-- UCF utilizado
-- Práctico entrenamiento nº03: Decodificador BCD.
-- Descripción: Se sintetiza en Lattice iCE2. 
-- 				Se simula en https://www.edaplayground.com/x/94Gm
-- Dificultades. Los pulsadores de la placa tienen lógica negada.
-- Colocar algún consejo para recordar en el futuro
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

 
ENTITY deco_bcd IS
    PORT(
         clock: IN  std_logic;
         reset : IN  std_logic;
         input : IN  std_logic_vector(3 downto 0);
         salida : OUT  std_logic_vector(9 downto 0)
        );
END deco_bcd;
 
ARCHITECTURE behavior OF deco_bcd IS

BEGIN

process (clock,reset) begin 
   if(reset='1')then
            salida <= "0000000000";
    elsif rising_edge(clock) then
        case input is
            when "0000" => salida <= "0000000001";
            when "0001" => salida <= "0000000010";
            when "0010" => salida <= "0000000100";
            when "0011" => salida <= "0000001000";
            when "0100" => salida <= "0000010000";
            when "0101" => salida <= "0000100000";
            when "0110" => salida <= "0001000000";
            when "0111" => salida <= "0010000000";
            when "1000" => salida <= "0100000000";
            when "1001" => salida <= "1000000000";
            when others => salida <= "0000000000";
        end case;
	end if;
    end process;

END;

