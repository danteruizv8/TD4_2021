----------------------------------------------------------------------------------
-- UNIVERSIDAD TECNOLÓGICA NACIONAL -FACULTAD REGIONAL CORDOBA
-- Carrera: INGENIERIA ELECTRÓNICA.
-- Asignatura: TÉCNICAS DIGITALES IV (ELECTIVA).
-- Año: 2021.
-- Grupo nº: XX.
-- Integrantes:
--
--		Ruiz, Dante Nelson				 Leg. nº:49881
--		Albarran, Gustavo.   		  	 Leg. nº:43143

--		
--
-- Fecha de Entrega: xx/xx/2021.
-- Hardware: Edu FPGA
-- VHDL auxiliares utilizados
-- UCF utilizado
-- Práctico entrenamiento nº03: Decodificador BCD.
-- Descripción: Se sintetiza en Lattice iCE2. 
-- 				Se simula en https://www.edaplayground.com/x/94Gm
-- Dificultades. Los pulsadores de la placa tienen lógica negada.
-- Colocar algún consejo para recordar en el futuro
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity deco_4to2_tb is
end deco_4to2_tb;

architecture tb of deco_4to2_tb is

    component deco_4to2
        port (clock  : in std_logic;
              reset  : in std_logic;
              input  : in std_logic_vector (3 downto 0);
              salida : out std_logic_vector (1 downto 0));
    end component;

    signal clock  : std_logic;
    signal reset  : std_logic;
    signal input  : std_logic_vector (3 downto 0);
    signal salida : std_logic_vector (1 downto 0);

    constant TbPeriod : time := 10 ns; -- Periodo
    signal TbClock : std_logic := '0';
    signal TbSimEnded : std_logic := '0';

begin

    dut : deco_4to2
    port map (clock  => clock,
              reset  => reset,
              input  => input,
              salida => salida);

    -- Clock generation
    TbClock <= not TbClock after TbPeriod/2 when TbSimEnded /= '1' else '0';

    -- EDIT: Check that clock is really your main clock signal
    clock <= TbClock;

    stimuli : process
    begin
        -- EDIT Adapt initialization as needed
        input <= (others => '0');

        -- Reset generation
        -- EDIT: Check that reset is really your reset signal
        reset <= '1';
        wait for 10 ns;
        reset <= '0';
        wait for 10 ns;

        -- EDIT Add stimuli here
        wait for 10 * TbPeriod;
        
         --- Estimulos      
      wait for (2)*TbPeriod;
      input <= "0001"; 
        
      wait for (2)*TbPeriod;
      input <= "0010"; 
        
      wait for (2)*TbPeriod;
      input <= "0100"; 
      
      wait for (2)*TbPeriod;
      input <= "1000"; 
      
      wait for (2)*TbPeriod;
      input <= "0101"; 
      
      wait for (2)*TbPeriod;
      input <= "0110"; 
      
      wait for (2)*TbPeriod;
      input <= "0111"; 
      
      wait for (2)*TbPeriod;
      input <= "1010"; 
      
      wait for (2)*TbPeriod;
      input <= "1001"; 
      
      wait for (2)*TbPeriod;
      input <= "1011"; 
      
      wait for (2)*TbPeriod;
      input <= "1111"; 
      
      wait for (2)*TbPeriod;
      input <= "1100"; 
      
      ---reset
      wait for TbPeriod;
      reset <= '1';

      wait for (2)*TbPeriod;
      input <= "0001"; 
      
      wait for (2)*TbPeriod;
      input <= "1010"; 

      wait for (2)*TbPeriod; 

        -- Stop the clock and end simulation
        TbSimEnded <= '1';
        wait;
    end process;

end tb;


