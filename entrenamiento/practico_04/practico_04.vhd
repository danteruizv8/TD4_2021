----------------------------------------------------------------------------------
-- UNIVERSIDAD TECNOLÓGICA NACIONAL -FACULTAD REGIONAL CORDOBA
-- Carrera: INGENIERIA ELECTRÓNICA.
-- Asignatura: TÉCNICAS DIGITALES IV (ELECTIVA).
-- Año: 2021.
-- Grupo nº: XX.
-- Integrantes:
--
--		Ruiz, Dante Nelson				 Leg. nº:49881
--		Albarran, Gustavo.   		  	 Leg. nº:43143

--		
--
-- Fecha de Entrega: xx/xx/2021.
-- Hardware: Edu FPGA
-- VHDL auxiliares utilizados
-- UCF utilizado
-- Práctico entrenamiento nº03: Decodificador BCD.
-- Descripción: Se sintetiza en Lattice iCE2. 
-- 				Se simula en https://www.edaplayground.com/x/a26f
-- Dificultades. Los pulsadores de la placa tienen lógica negada.
-- Colocar algún consejo para recordar en el futuro
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

 
ENTITY deco_4to2 IS
    PORT(
         clock: IN  std_logic;
         reset : IN  std_logic;
         input : IN  std_logic_vector(3 downto 0);
         salida : OUT  std_logic_vector(1 downto 0)
        );
END deco_4to2;
 
ARCHITECTURE behavior OF deco_4to2 IS

BEGIN

process (clock,reset) begin 
   if(reset='1')then
            salida <= "00";
    elsif rising_edge(clock) then
--    	if input="0001" then salida <= "00";
--    	elsif input="0001" then salida <= "01";
--    	elsif input="0010" then salida <= "01";
--    	elsif input="0100" then salida <= "10";
--    	elsif input="1000" then salida <= "11";
--    	else salida <= "ZZ";
--    	end if;
    	
    	-- Para entradas Pull Up (Edu FPGA)
    	if input=not "0001" then salida <= "00";
    	elsif input=not "0001" then salida <= "01";
    	elsif input=not "0010" then salida <= "01";
    	elsif input=not "0100" then salida <= "10";
    	elsif input=not "1000" then salida <= "11";
    	else salida <= "ZZ";
    	end if;
    	        
--        case input is  
--            when "0001" => salida <= "00";
--            when "0010" => salida <= "01";
--            when "0100" => salida <= "10";
--            when "1000" => salida <= "11";
--            when others => salida <= "ZZ";
--        end case;
	end if;
end process;

END;

